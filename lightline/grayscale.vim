" =============================================================================
" Filename: autoload/lightline/colorscheme/grayscale.vim
" Author: R O
" License: MIT License
" Last Change: 2021/04/22
" =============================================================================

" Warnings and errors
let s:brightred         =   [ 160 ]
let s:brightestred      =   [ 196 ]
let s:yellow            =   [ 136 ]

" The rest
let s:bkg               =   [ 234 ]
let s:white             =   [ 231 ]
let s:gray0             =   [ 233 ]
let s:gray1             =   [ 235 ]
let s:gray2             =   [ 236 ]
let s:gray3             =   [ 239 ]
let s:gray4             =   [ 240 ]
let s:gray5             =   [ 241 ]
let s:gray6             =   [ 244 ]
let s:gray7             =   [ 245 ]
let s:gray8             =   [ 247 ]
let s:gray9             =   [ 250 ]
let s:gray10            =   [ 252 ]

let s:p                 =   {'normal': {}, 'inactive': {}, 'insert': {}, 'replace': {}, 'visual': {}, 'tabline': {}}

let s:p.normal.left     =   [ ['white', 'bkg', 'bold'], ['white', 'bkg'], [ 'white', 'red' ] ]
let s:p.normal.middle   =   [ [ 'white', 'bkg' ] ]
let s:p.normal.right    =   [ ['white', 'bkg'], ['white', 'bkg'], ['gray8', 'gray2'] ]
let s:p.normal.error    =   [ [ 'gray9', 'brightestred' ] ]
let s:p.normal.warning  =   [ [ 'gray1', 'yellow' ] ]

let s:p.insert.left     =   [ ['gray3', 'gray9', 'bold'], ['gray10', 'gray6'], [ 'white', 'red' ] ]
let s:p.insert.middle   =   [ [ 'gray7', 'gray3' ] ]
let s:p.insert.right    =   [ [ 'white', 'gray9' ], [ 'gray10', 'gray6' ], [ 'gray9', 'white' ] ]

let s:p.visual.left     =   [ ['white', 'bkg', 'bold'], ['white', 'bkg'], [ 'white', 'red' ] ]
let s:p.visual.middle   =   [ ['white', 'gray3' ] ]
let s:p.visual.right    =   [ [ 'white', 'bkg' ], [ 'white', 'bkg' ] ]

let s:p.inactive.right  =   [ ['gray1', 'gray5'], ['gray4', 'gray1'], ['gray4', 'gray0'] ]
let s:p.inactive.left   =   s:p.inactive.right[1:]

let s:p.replace.left    =   [ ['white', 'brightred', 'bold'], ['white', 'gray4'] ]
let s:p.replace.middle  =   s:p.normal.middle
let s:p.replace.right   =   s:p.normal.right

let s:p.tabline.left    =   [ [ 'gray9', 'gray4' ] ]
let s:p.tabline.middle  =   [ [ 'gray2', 'gray8' ] ]
let s:p.tabline.right   =   [ [ 'gray9', 'gray3' ] ]
let s:p.tabline.tabsel  =   [ [ 'gray9', 'gray1' ] ]

let g:lightline#colorscheme#grayscale#palette = lightline#colorscheme#fill(s:p)
